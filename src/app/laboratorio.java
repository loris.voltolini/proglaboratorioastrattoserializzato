package app;


import java.io.*;
import java.io.Serializable;
import java.util.ArrayList;

public class laboratorio implements Serializable {
    private ArrayList <Pc> elenco = new ArrayList<Pc>();

    public laboratorio()
        {
            elenco= new ArrayList<Pc>();
        }
    public laboratorio(laboratorio l)
        {
            this();
            for(Pc p: elenco)
                {
                    elenco.add(p.clone());
                }
        }
    public void InserisciPc(Pc p)
        {
            elenco.add(p.clone());
        }

    public void eliminaPc(Pc p)
        {
            elenco.remove(p);
        }
        
    public void aggiustaPcGuasti()
        {
            for(Pc p: elenco)
            {
                if(p.getStato()==true)
                    {
                        p.setStato(false);
                    }
            } 
        }
    
    public String StampaGuasti()
        {
            String s="";
            for(Pc p: elenco)
                {
                    if(p.getStato()==true)
                        {
                            s+= p.toString();
                        }
                    
                }
            return s;
        }
        public void SalvaLab() throws java.io.IOException 
        {
            final FileOutputStream f = new FileOutputStream(new File("lab.bin"));
            ObjectOutputStream o = new ObjectOutputStream(f);
            
            o.writeObject(this.elenco);
            f.close();
                    
            
        }

    public void caricaLab() throws java.io.IOException 
    {

        FileInputStream fi = new FileInputStream(new File("lab.bin"));
        ObjectInputStream oi = new ObjectInputStream(fi);
        try
            {
                this.elenco = (ArrayList<Pc>) oi.readObject();
            }
        catch (ClassNotFoundException exception)
            {
                oi.close();
            }
        
    }
    @Override
    public String toString()
    {
        String s="";
        for(Pc p: elenco)
            {
                s+= p.toString();
            }
        return s;
    }
    
}
