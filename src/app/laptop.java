package app;

public class laptop extends Pc {
    //attributi
    private int durataBatteria;

    public laptop(){}
    public laptop(String marca, String tipo, int memoria, boolean guasto, int durataBatteria)
        {
            super(marca,tipo,memoria,guasto);
            this.setMarca(marca);
            this.setTipo(tipo);
            this.setMemoria(memoria);
            this.setStato(guasto);
            this.durataBatteria=durataBatteria;
        }
    public laptop(laptop l)
        {
            super();
            this.setMarca(l.getMarca());
            this.setMemoria(l.getMemoria());
            this.setTipo(l.getTipo());
            this.setStato(l.getStato());
            this.durataBatteria=l.getDurataBatteria();
        }
    
    public int getDurataBatteria() {return this.durataBatteria;}
    public void setDurataBatteria(int durataBatteria) {this.durataBatteria = durataBatteria;}
    
    @Override
    public Pc clone()
        {
            return new laptop(this.getMarca(), this.getTipo(),this.getMemoria(), this.getStato(),this.getDurataBatteria());
        }
    @Override
    public  boolean equals(Pc p)
        {
            return getMarca().equals(p.getMarca()) && getTipo().equals(p.getTipo()) && getMemoria()==p.getMemoria() && getStato()==p.getStato() && getDurataBatteria()==((laptop) p).getDurataBatteria();
        }
    
    @Override
    public String toString()
    {
        if(getStato()==false)
            {
                return "laptop funzionante" + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Durata Batteria: " + getDurataBatteria() +  System.lineSeparator();
            }
         else
            {
                return "laptop non funzionante " + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Durata Batteria: " + getDurataBatteria() + System.lineSeparator();
            }
    }
    
}
