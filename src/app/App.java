package app;
public class App {
    public static void main(String[] args) throws Exception {
        laboratorio lab = new laboratorio();

        laptop l= new laptop("Dell", "gino", 63, true, 4);
        laptop l1= new laptop("Dell", "sdf", 56, true, 3);
        laptop l2= new laptop("Dell", "gisdfsno", 12, false, 6);
        laptop l3= l1;
        laptop l4=new laptop(l2);
        lab.InserisciPc(l);
        lab.InserisciPc(l1);
        lab.InserisciPc(l2);
        lab.InserisciPc(l3);
        lab.InserisciPc(l4);
        Desktop d= new Desktop("Dell", "gino", 78, true, 6.7);
        Desktop d1= new Desktop("Dell", "gino", 9, false, 6.7);
        Desktop d2= new Desktop("Dell", "gino", 89, false, 6.7);
        Desktop d3= d1;
        Desktop d4= new Desktop(d1);
        lab.InserisciPc(d);
        lab.InserisciPc(d1);
        lab.InserisciPc(d2);
        lab.InserisciPc(d3);
        lab.InserisciPc(d4);

        System.out.println(lab.toString());
        lab.eliminaPc(l1);
        lab.eliminaPc(d1);
        System.out.println(lab.toString());
        System.out.println(lab.StampaGuasti());
        System.out.println(lab.toString());
        lab.SalvaLab();
        laboratorio lab2= new laboratorio();
        lab2.caricaLab();
        System.out.println(lab2.toString());
    }
}
