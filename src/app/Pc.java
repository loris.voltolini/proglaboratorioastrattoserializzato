package app;

import java.io.Serializable;

abstract class Pc implements Serializable{
    private String marca;
    private String tipo;
    private  int memoria;
    private boolean guasto;

    public Pc(){}
    public Pc(String mmarca, String tipo, int memoria, boolean guasto)
        {
            this.marca=marca;
            this.tipo=tipo;
            this.memoria=memoria;
            this.guasto=guasto;
        }
    public Pc(Pc p)
        {
            this.marca=p.getMarca();
            this.tipo=p.getTipo();
            this.memoria=p.getMemoria();
            this.guasto=p.getStato();
        }
    public String getMarca() {return this.marca;}
    public void setMarca(String marca) {this.marca = marca;}
    public String getTipo() {return this.tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}
    public int getMemoria() {return this.memoria;}
    public void setMemoria(int memoria) {this.memoria = memoria;}
    public boolean getStato() {return this.guasto;}
    public void setStato(boolean guasto) {this.guasto=guasto;}

    @Override
    public boolean equals(Object obj)
        {
        if(obj instanceof Pc)
                {
                    return equals((Pc) obj);
                }
            else
                {
                    return false;
                }
        
        }
    //metodi astratti 
    public abstract Pc clone();
   
    public  boolean equals(Pc p)
        {
            return getMarca().equals(p.getMarca()) && getTipo().equals(p.getTipo()) && getMemoria()==p.getMemoria() && getStato()==p.getStato();
        }

    @Override
    public String toString()
        {
            if(getStato()==false)
                {
                    return "Pc funzionante" + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + System.lineSeparator();
                }
            else
                {
                    return "Pc non funzionante " + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + System.lineSeparator();
                }
        
        }
    
}
